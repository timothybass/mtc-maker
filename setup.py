from setuptools import setup

setup(
    name='suned-gencon',
    version='1.2',
    packages=['sunedgencon'],
    include_package_data=True,
    install_requires=[
        'click',
        'click-log',
        'logging',
        'nose',
        'lxml',
        'requests'
    ],
    entry_points='''
        [console_scripts]
        gencon=sunedgencon.main:cli
    ''',
)
