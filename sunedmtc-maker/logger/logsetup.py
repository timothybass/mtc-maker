import os
import sys
import errno
import logging

__author__ = 'jim.graf'


def log_setup(logging_level, log_name, error_log_name):
    # set up logging to file

    if not os.path.exists(os.path.dirname('./logs')):
        try:
            os.makedirs(os.path.dirname('./logs'))
        except OSError as exc:  # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise

    log_file_name = _add_log_folder_to_name(log_name)
    error_log_file_name = _add_log_folder_to_name(error_log_name)

    level = _get_logging_level(logging_level)
    logging.basicConfig(level=level,
                        format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                        datefmt='%m-%d %H:%M',
                        filename=log_file_name,
                        filemode='w')
    root_logger = logging.getLogger('')
    # define a Handler which writes INFO messages or higher to the sys.stderr
    console = logging.StreamHandler()
    console.setLevel(level)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
    console.setFormatter(formatter)
    root_logger.addHandler(console)

    errorFormatter = logging.Formatter('%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
    error_handler = logging.FileHandler(error_log_file_name)
    error_handler.setLevel(logging.ERROR)
    error_handler.setFormatter(errorFormatter)
    root_logger.addHandler(error_handler)

def _get_logging_level(logging_level):
    return {
        'NOTSET': logging.NOTSET,
        'DEBUG': logging.DEBUG,
        'INFO': logging.INFO,
        'WARNING': logging.WARNING,
        'ERROR': logging.ERROR,
        'CRITICAL': logging.CRITICAL
    }[logging_level.upper()]

def _add_log_folder_to_name(log_name):
    return './logs/{name}'.format(name=log_name)