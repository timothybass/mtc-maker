import click
import click_log
import datetime
import logging
import os
import sys
import csv

from sunedgencon.logger import logsetup
from sunedgencon.csvprocessor.CsvProcessor import CsvProcessor
from sunedgencon.connectiongenerator.ConnectionGenerator import ConnectionGenerator
from sunedgencon.connectiongenerator.getsession import get_session_id


@click.command()
@click.argument('input_path', type=click.Path(exists=True))
@click.option('-url', default='https://app3.informaticacloud.com/saas/api/v2/connection/', help='The URL to connect to')
@click.option('-login-url', default='https://app.informaticaondemand.com/ma/api/v2/user/login', help='The login URL to connect to')
@click.option('-ll', '--logging-level', default='INFO',
              help='The logging level to log: DEBUG, INFO, WARNING, ERROR, CRITICAL')
@click.option('-dr/-ndr', '--dry-run/--no-dry-run', default=False, help='Perform a dry run so we do not create connections')

@click_log.simple_verbosity_option()
@click_log.init(__name__)

def cli(input_path, url, login_url, logging_level, dry_run):
    start_time = datetime.datetime.now()

    timestamp = start_time.strftime('%Y%m%d-%H%M%S')
    log_name = 'main-{timestamp}.log'.format(timestamp=timestamp)
    error_log_name = 'main-error-{timestamp}.log'.format(timestamp=timestamp)

    logsetup.log_setup(logging_level=logging_level, log_name=log_name, error_log_name=error_log_name)

    logging.info('Reading from path {path}'.format(path=input_path))

    csv.field_size_limit(2147483647)

    files = find_files(input_path)

    processor = CsvProcessor()

    databases = []

    for file in files:
        databases += processor.process(file)

    icSessionId = get_session_id(login_url=login_url, login_credentials='ONDEMAND')
    logging.info('Got session ID: {icSessionId}'.format(icSessionId=icSessionId))

    for database in databases:
        conngen = ConnectionGenerator()
        conngen.connect(url=url, icSessionId=icSessionId, row=database, dry_run=dry_run)

    finish_time = datetime.datetime.now()
    total_time = finish_time - start_time
    logging.info("--- Total time: {total_time} ---".format(total_time=total_time))
    exit(0)


def find_files(directory):
    files = []
    for root, subdirs, filenames in os.walk(directory):
        for subdir in subdirs:
            files.append(find_files(subdir))
        for filename in filenames:
            logging.info("Adding {filename} to queue".format(filename=filename))
            file_path = os.path.join(root, filename)
            if isinstance(file_path, basestring):
                files.append(file_path)

    return files

if __name__ == '__main__':
    cli(sys.argv[1:])
