__author__ = 'jim.graf'

import codecs
import csv
import logging
import re

from csv import DictReader


class CsvProcessor:

    def __init__(self):
        pass

    def process(self, csv_file_name):
        databases = []
        with open(csv_file_name, 'r') as csv_file:
            logging.debug('Opened file: {csv_file_name}'.format(csv_file_name=csv_file_name))
            reader = DictReader(f=csv_file, dialect=csv.excel)

            for row in reader:
                name = self._get_name(row)
                type = self._get_value(row, 'type')
                credentials = self._get_value(row, 'credentials')
                host = self._get_value(row, 'host')
                port = self._get_value(row, 'port')
                database = self._get_value(row, 'database')
                schema = self._get_schema(row)

                databases.append({'name' : name,
                                  'type' : type,
                                  'credentials' : credentials,
                                  'host' : host,
                                  'port' : port,
                                  'schema' : schema,
                                  'database' : database
                                  })

        return databases

    def _get_name(self, row):
        name = self._get_value(row, 'name')
        if name.startswith(codecs.BOM_UTF8):
            name = name[3:]

        return name

    def _get_schema(self, row):
        if 'schema' in row:
            return self._get_value(row, 'schema')

        return None

    def _get_value(self, row, column_name):
        if column_name in row:
            element_value = row[column_name].strip()
            return element_value

        return ''
