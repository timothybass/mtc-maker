__author__ = 'jim.graf'

import os
from ConfigParser import SafeConfigParser

def get_credentials(login_credentials):
    cfg = './conf/creds.properties'
    cfg_path = os.path.realpath(cfg)
    config = SafeConfigParser()
    config.read(cfg_path)
    user = config.get(login_credentials, 'user')
    password = config.get(login_credentials, 'password')
    return {'user': user, 'password': password}