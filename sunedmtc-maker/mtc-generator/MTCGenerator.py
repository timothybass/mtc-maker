__author__ = 'jim.graf'

import logging
import requests

from XmlGenerator import XmlGenerator
from getcredentials import get_credentials
from xml.sax.saxutils import escape


class ConnectionGenerator:
    xml_generator = XmlGenerator()

    def __init__(self):
        pass

    def connect(self, url, icSessionId, row, dry_run):
        connection_name = row['name']
        database = row['database']
        database_type = row['type']
        database_cred = row['credentials']
        database_host = row['host']
        database_port = row['port']
        database_schema = row['schema']

        logging.info('Connecting {connection_name} to {schema} of type {type} on {host}:{port} using credentials from {credentials}'.format(
            connection_name=connection_name, schema=database_schema, type=database_type, host=database_host,
            port=database_port, credentials=database_cred))

        credentials = get_credentials(database_cred)

        connection_details = {
            'name': connection_name,
            'type': database_type,
            'username': escape(credentials['user']),
            'password': escape(credentials['password']),
            'database': database,
            'ordId': '001MUP',
            'host': database_host,
            'port': database_port,
            'codepage': 'UTF-8',
            'agentId': '001MUP08000000000002'
        }

        if database_schema is not None:
            connection_details['schema'] = database_schema

        payload = self.xml_generator.generate_connection_request(connection_details)

        headers = {
            'content-type': "application/xml",
            'accept': "application/xml",
            'icsessionid': icSessionId,
            'cache-control': "no-cache"
        }

        if dry_run:
            logging.info(payload)
            logging.info('Just kidding dry run')
            return

        response = requests.request('POST', url, data=payload, headers=headers)
        logging.info(response)

        if response.status_code == requests.codes.ok:
            logging.info('SUCCESS: {database_name}'.format(database_name=connection_name))
        else:
            logging.error('FAILED: {database_name}'.format(database_name=connection_name))
            logging.error('Cause: {content}'.format(content=response.content))
