import logging
import json
import requests

from getcredentials import get_credentials

def get_session_id(login_url, login_credentials):
    credentials = get_credentials(login_credentials)
    login_request_payload = _get_login_request_payload(credentials)

    session = requests.session()
    res = session.post(login_url, json=login_request_payload)
    logging.info('Response: {res}'.format(res=res))
    content = json.loads(res.content)

    return content['icSessionId']


def _get_login_request_payload(credentials):

    credentials = {
        '@type': 'login',
        'username': credentials['user'],
        'password': credentials['password']
    }

    return credentials

