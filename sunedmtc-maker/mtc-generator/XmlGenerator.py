__author__ = 'jim.graf'

import logging
import re

from lxml import etree
from lxml.etree import Element

class XmlGenerator:

    def __init__(self):
        pass

    def generate_connection_request(self, connection_details):
        connectionEl = Element('connection')
        for k, v in connection_details.iteritems():
            self.add_element(connectionEl, k, v)

        return etree.tostring(connectionEl, pretty_print=True)

    def add_element(self, parent, element_name, element_value):
        el = Element(element_name)
        el.text = self.escape_xml_string(element_value)
        parent.append(el)

    def escape_xml_string(self, element_value):
        element_value = re.sub(u'[^\u0020-\uD7FF\u0009\u000A\u000D\uE000-\uFFFD\u10000-\u10FFFF]+', '', element_value)
        element_value = ''.join(c for c in element_value if self.valid_xml_char_ordinal(c))
        return element_value

    def valid_xml_char_ordinal(self, c):
        codepoint = ord(c)
        # conditions ordered by presumed frequency
        return (
            0x20 <= codepoint <= 0xD7FF or
            codepoint in (0x9, 0xA, 0xD) or
            0xE000 <= codepoint <= 0xFFFD or
            0x10000 <= codepoint <= 0x10FFFF
        )